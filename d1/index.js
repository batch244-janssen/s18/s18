console.log("Hello World");

// [ SECTION ] Functions | Parameters
	//DRY Principle - (Don't Repeat Yourself) is a principle of a software development.

	function printInput(nickname) {
		console.log ('Hi, ' + nickname);
	}

	printInput("Carlo");
	printInput("Carla");
	printInput("Carlk");
	printInput("Carda");



	// "nickname is called parameter"

		 let sampleVariable = "Cardi";
		 printInput(sampleVariable);

	function checkDivisibility8(inputNumber){
		let remainder = inputNumber % 8;
		console.log('The remainder of ' + inputNumber + ' divided by 8 is: ' + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log ('Is ' + inputNumber + ' divisible by 8?');
		console.log(isDivisibleBy8);
		console.log(typeof isDivisibleBy8);
	}

	checkDivisibility8(56);
	checkDivisibility8(28);

	function printInt(){
		let number = prompt(' Enter a number');
		console.log(number);
		console.log(typeof number);
	}
	printInt();


//	[SECTION] Function | Arguments
	//Some complex functions use other functions as arguements to perform more complicated result

	function arguementFunction() {
		console.log('This was passes as an argument before the message was printed.');
	};

	function invokeFunction(arguementFunction){
		arguementFunction();
	};

	invokeFunction(arguementFunction);

	// console.log(arguementFunction);

	//No parameter, No arguement 

// [ SECTION ] Functions | Using Multiple Parameter 
	// Multiple 'Arguments' will correspond to the number of 'parameters' declared in a function in succedding order.

	function createFullName(firstName, middleName, lastName) {
		console.log(firstName + " " + middleName + " " + lastName);
		
	};

	createFullName('Juan' , 'Dela', "Cruz");

		//Using Variables as arguements
			let firstName = 'Mike';
			let middleName = "James";
			let lastName = "Ross";

	createFullName('Harvey'



		, 'A', 'Specer');

	createFullName(firstName, middleName, lastName);

//[ SECTION ] Return Statements
	//The 'return' statement allows us to output a value from a function to be passed on the line/block of code that invoked/called the function.

	function returnFullName(firstName, middleName, lastName){

		
		console.log('i love Javascript!');
		console.log('i love Javascript!');
		console.log('i love Javascript!');
		console.log('i love Javascript!');
		return firstName + "  " + middleName + " " + lastName;
	}
	let completeName = returnFullName('Jeffrey', "Smitch", "Bezos");

		console.log(completeName);
		console.log(typeof completeName);

	function returnAddress(city, country) {
		let fullAddress = city + " , " + country;
		return fullAddress;
	};

	let myAddress = returnAddress ('Cebu City', 'Philippines');

	console.log(myAddress);



